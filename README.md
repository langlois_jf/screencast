# Screencast Utils

This is a simple script for taking screencasts with ffmpeg with minimal storage
requirements. It uses QTRLE for video with I-frames far apart (the assumption
being that the screen will not change that often.) Audio is encoded into FLAC.

An encode pass will convert the video to VP9 and the audio to Opus, in a WebM
container. An optional audio cleanup script will take a 1 second sample at the
beginning of the video, assuming that it is background noise, and use `sox` to
remove the noise from the audio.

## Running

To run the capture, simply do:

    $ ./capture.sh <dir_prefix>  # Press q to stop the recording
    $ ./audio-cleanup.sh <dir_prefix>  # Optional
    $ ./encode.sh <dir_prefix>

Where the `dir_prefix` is the prefix of the name of the directory where all
output will go. You need to have the `ffmpeg` command somewhere in your `$PATH`.
(As well as sox for the audio cleanup.)

## With Nix

If you have the [Nix](https://nixos.org/nix/) package manager available, you
can run the command with:

    $ nix-shell --command "./capture.sh <dir_prefix>"
    $ nix-shell --command "./audio-cleanup.sh <dir_prefix>"
    $ nix-shell --command "./encode.sh <dir_prefix>"

Which will take care of the `ffmpeg` and `sox` dependencies for you.
