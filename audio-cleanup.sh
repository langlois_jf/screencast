#!/bin/sh

if [[ "$#" -ne 1 ]]; then
  echo "1 argument required, $# provided"
  exit 1
fi
if [[ -z $1 ]]; then
  echo "argument must be non-empty"
  exit 2
fi

OUT_DIR=$1-screencast

ffmpeg -i $OUT_DIR/audio.flac -vn -ss 00:00:00 -t 00:00:01 $OUT_DIR/noise.wav
sox $OUT_DIR/noise.wav -n noiseprof $OUT_DIR/noise.prof
sox $OUT_DIR/audio.flac $OUT_DIR/audio-clean.flac noisered $OUT_DIR/noise.prof 0.21
mv $OUT_DIR/audio.flac $OUT_DIR/audio-noisy.flac
mv $OUT_DIR/audio-clean.flac $OUT_DIR/audio.flac
