#!/bin/sh

if [[ "$#" -ne 1 ]]; then
  echo "1 argument required, $# provided"
  exit 1
fi
if [[ -z $1 ]]; then
  echo "argument must be non-empty"
  exit 2
fi

OUT_DIR=$1-screencast
mkdir $OUT_DIR
ffmpeg \
  -thread_queue_size 512 -video_size 1920x1080 -framerate 30 -f x11grab -i :0.0 \
    -c:v qtrle -g 2400 -an $OUT_DIR/video.mov \
  -thread_queue_size 512 -f pulse -i default -ac 1 \
    -c:a flac -vn $OUT_DIR/audio.flac
