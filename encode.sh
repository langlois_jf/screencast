#!/bin/sh

if [[ "$#" -ne 1 ]]; then
  echo "1 argument required, $# provided"
  exit 1
fi
if [[ -z $1 ]]; then
  echo "argument must be non-empty"
  exit 2
fi

OUT_DIR=$1-screencast

ffmpeg \
  -i $OUT_DIR/video.mov \
  -c:v libvpx-vp9 -pass 1 -passlogfile "$OUT_DIR/passlog" -b:v 0 -crf 33 -threads 8 -speed 4 -tile-columns 4 \
    -keyint_min 60 -g 60 \
    -frame-parallel 1 -an -f webm /dev/null -y
ffmpeg \
  -i $OUT_DIR/video.mov -i $OUT_DIR/audio.flac \
  -c:v libvpx-vp9 -pass 2 -passlogfile "$OUT_DIR/passlog" -b:v 0 -crf 33 -threads 8 -speed 2 -tile-columns 4 \
    -keyint_min 60 -g 60 \
    -frame-parallel 1 -auto-alt-ref 1 -lag-in-frames 25 \
  -c:a libopus -application voip \
  -f webm $OUT_DIR/$1.webm
