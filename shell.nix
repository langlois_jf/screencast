with import <nixpkgs> {}; {
  screencastEnv = stdenv.mkDerivation {
    name = "screencast";
    buildInputs = [ ffmpeg sox ];
  };
}
